package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private Controller controller;

    private Logger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public View() {
        controller = new ControllerImp();
        logger = LogManager.getLogger(View.class);

        input = new Scanner(System.in);

        setMenu();

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1",this::getCoordinatesAndSizeOfSequences);
        methodsMenu.put("2",this::printMinesWeeper);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - print annotate fields");
        menu.put("2", "2 - print invoke methods");
        menu.put("3", "3 - set value to unknowing field");
        menu.put("4", "4 - get information");
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

    private void getCoordinatesAndSizeOfSequences(){
        logger.info(controller.getCoordinatesAndSizeOfSequences());
    }
    private void printMinesWeeper(){
        //logger.info(controller.printMinesWeeper(input.nextInt(),input.nextInt(),input.nextDouble()));
        logger.info(controller.printMinesWeeper(4,4,0.5));
    }
}
