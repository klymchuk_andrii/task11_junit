package com.klymchuk.model;


public class MinesWeeper {
    private int M;
    private int N;
    private double p;
    private String[][] array;

    public MinesWeeper(int mParam, int nParam, double pParam) {
        M = mParam;
        N = nParam;
        p = pParam;
        array = new String[M][N];
    }

    public void createArray() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (Math.random() > p) {
                    array[i][j] = "-";
                } else {
                    array[i][j] = "*";
                }
            }
        }
    }

    public void countBomb() {
        String[][] mas = new String[M + 2][N + 2];

        int count = 0;

        for (int i = 0; i < M + 2; i++) {
            for (int j = 0; j < N + 2; j++) {
                mas[i][j] = " ";
            }
        }

        for (int i = 0; i < M; i++) {
            if (N >= 0) System.arraycopy(array[i], 0, mas[i + 1], 1, N);
        }

        for (int i = 1; i < M+1; i++) {
            for (int j = 1; j < N+1; j++) {
                if (mas[i][j].equals("-")) {
                    count = 0;
                    for (int i1 = i - 1; i1 < i + 2; i1++) {
                        for (int j1 = j - 1; j1 < j + 2; j1++) {
                            if (mas[i1][j1].equals("*")) {
                                count++;
                            }
                        }
                    }
                    array[i-1][j-1] = String.valueOf(count);
                }
            }
        }
    }

    public String getStringOfArray() {
        StringBuilder stringBuilder = new StringBuilder();


        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                stringBuilder.append(" ").append(array[i][j]);
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
