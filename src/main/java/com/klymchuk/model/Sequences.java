package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;

public class Sequences {
    private List<Integer> list;
    private int startOfSequence;
    private int endOfSequence;

    public Sequences() {
        list = new ArrayList<>();
        startOfSequence = 0;
        endOfSequence = 0;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public void findCoordinatesOfLargestSequence() {
        int end;
        for (int i = 0; i < list.size() - 1; i++) {
            end = i + 1;
            while (list.get(i).equals(list.get(end))&&end<list.size()-1) {
                end++;
            }
            if (end != i + 1) {
                if (checkLimitsOfSequences(i, end-1)) {
                    if (end - i > endOfSequence - startOfSequence) {
                        startOfSequence = i;
                        endOfSequence = end;
                    }
                }
            }
            i=end-1;
        }
    }

    private boolean checkLimitsOfSequences(int start, int end) {
        if (start == 0 && end == list.size() - 2) {
            return true;
        } else if (start == 0 && end != list.size() - 2) {
            return list.get(end) > list.get(end + 1);
        } else if (start != 0 && end == list.size() - 2) {
            return list.get(start) > list.get(start - 1);
        } else {
            return list.get(start) > list.get(start - 1) && list.get(end) > list.get(end + 1);
        }
    }

    public int[] getCoordinatesAndSizeOfSequences() {
        findCoordinatesOfLargestSequence();
        int[] answer = new int[3];
        answer[0] = startOfSequence;
        answer[1] = endOfSequence;
        answer[2] = endOfSequence - startOfSequence;
        return answer;
    }
}
