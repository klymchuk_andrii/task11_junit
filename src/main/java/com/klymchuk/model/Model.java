package com.klymchuk.model;

import java.util.List;

public interface Model {
    String getCoordinatesAndSizeOfSequences(List<Integer> list);
    String printMinesWeeper(int M,int N,double p);
}
