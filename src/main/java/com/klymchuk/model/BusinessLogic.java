package com.klymchuk.model;

import java.util.List;

public class BusinessLogic implements Model {
    private Sequences sequences;
    private MinesWeeper minesWeeper;

    public BusinessLogic() {
        sequences = new Sequences();
    }

    @Override
    public String getCoordinatesAndSizeOfSequences(List<Integer> list) {
        sequences.setList(list);

        StringBuilder stringBuilder = new StringBuilder();

        int[] coordinates = new int[3];

        coordinates = sequences.getCoordinatesAndSizeOfSequences();

        stringBuilder.append("start of sequences: ").append(coordinates[0]);
        stringBuilder.append(" end of sequences: ").append(coordinates[1]);
        stringBuilder.append("\n size: ").append(coordinates[2]);

        return stringBuilder.toString();
    }

    @Override
    public String printMinesWeeper(int M, int N, double p) {
        minesWeeper = new MinesWeeper(M, N, p);
        minesWeeper.createArray();
        String answer = minesWeeper.getStringOfArray();
        answer += "\n";
        minesWeeper.countBomb();
        answer += minesWeeper.getStringOfArray();
        return answer;
    }
}
