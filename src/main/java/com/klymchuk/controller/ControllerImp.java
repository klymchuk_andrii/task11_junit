package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

import java.util.ArrayList;
import java.util.List;

public class ControllerImp implements Controller{
    private Model model;

    public ControllerImp(){
        model = new BusinessLogic();
    }

    @Override
    public String getCoordinatesAndSizeOfSequences() {
        List<Integer> list = new ArrayList<>();
        for (int i=0;i<5;i++){
            if(i==4){
                for(int j=0;j<5;j++){
                    list.add(9);
                }
            }else if(i==2){
                for(int j=0;j<5;j++){
                    list.add(i);
                }
            }else if(i==0){
                for(int j=0;j<3;j++){
                    list.add(7);
                }
            }else {
                list.add(i);
            }
        }
        return model.getCoordinatesAndSizeOfSequences(list);
    }

    @Override
    public String printMinesWeeper(int M, int N, double p) {
        return model.printMinesWeeper(M,N,p);
    }
}
