package com.klymchuk.controller;

import java.util.List;

public interface Controller {
    String getCoordinatesAndSizeOfSequences();
    String printMinesWeeper(int M,int N,double p);
}
