package com.klymchuk;

import com.klymchuk.model.Sequences;
import org.junit.Test;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class SequencesTest {

    @BeforeAll
    public static void beforeAll(){
        System.out.println("@beforeAll - run before all");
    }

    @BeforeEach
    public void init(){
        System.out.println("@beforeEach - run");
    }

    @Test
    public void test1(){
        Sequences sequences =new Sequences();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        sequences.setList(list);
        int[] value = sequences.getCoordinatesAndSizeOfSequences();
        assertEquals(value[0],0);
        assertEquals(value[0],0);
        System.out.println("start: "+value[0]+" end: "+value[1]);
    }

    @Test
    @Disabled
    public void disabledTest(){
        System.out.println("disabled test");
    }

    @AfterEach
    public void tearDown(){
        System.out.println("@afterEach - after");
    }
    @AfterAll
    public static void tearDownAll(){
        System.out.println("@afterAll - after all");
    }
}
