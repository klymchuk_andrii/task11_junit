package com.klymchuk;

import com.klymchuk.model.MinesWeeper;
import org.junit.jupiter.api.RepeatedTest;


public class MyRepeatedTest {
    int count=0;

    @RepeatedTest(2)
    public void mines(){
        int[][] arr = {{3,3},{5,5}};
        double[] arrP = {0.5,0.7};

        MinesWeeper minesWeeper = new MinesWeeper(arr[count][0],arr[count][1],arrP[count]);
        minesWeeper.createArray();
        minesWeeper.countBomb();
        System.out.println(minesWeeper.getStringOfArray());

        count++;
    }
}
